FROM quay.io/centos/centos:stream9

ARG shortbuild

RUN dnf install -y \
	dnf-plugin-config-manager \
	epel-release
RUN dnf config-manager -y --set-enabled crb

RUN if [ "$shortbuild" = "" ] ; then \
	dnf config-manager --add-repo https://cli.github.com/packages/rpm/gh-cli.repo; \
	dnf -y copr enable @centos-automotive-sig/bluechi-snapshot centos-stream-9;  \
	dnf copr enable -y rhcontainerbot/qm centos-stream-9-$(arch); \
    fi

RUN dnf install -y \
	gcc \
	make \
	clang-tools-extra \
	g++ \
	fpaste \
	lcov \
	hostname \
	podman \
	pcp \
	sudo \
	gpgme-devel \
	libseccomp-devel \
	iptables-services \
	selinux-policy-targeted \
	selinux-policy-devel \
	systemd \
	gzip \
	jq \
	container-selinux \
	kernel \
	kernel-modules \
	passwd \
	python3-pip \
	meson \
	rpm-build \
	sed \
	systemd-devel \
	iputils \
	tar \
	telnet \
	net-tools \
	iproute \
	ioping

RUN if [ "$shortbuild" = "" ] ; then \
	dnf install -y \
	golang-github-cpuguy83-md2man \
	vim-enhanced \
	ruby \
	npm \
	qm \
	git \
	python3-gobject \
	tmt \
	valgrind \
	createrepo_c \
	dnf-utils \
	ShellCheck \
	python3-podman \
	python3-flake8 \
	python3-pytest \
	python3-pytest-timeout \
	pre-commit \
	gh \
	go-toolset \
	bluechi \
	bzip2 \
	cargo \
	rust \
	codespell \
        bluechi-ctl ;\
    fi

RUN alternatives --install /usr/bin/python python /usr/bin/python3 1
#RUN npm install markdownlint-cli2 --global
RUN pip install dasbus

# Makefile
RUN curl --create-dirs -O --output-dir /root/tests/FFI/ https://raw.githubusercontent.com/containers/qm/main/tests/e2e/tools/FFI/Makefile

# update Makefile to add shm
RUN sed -i '/CFLAGS = -Wall -g/a CFLAGS_SHM = -lrt -Wall -Werror' /root/tests/FFI/Makefile
RUN sed -i '/VPATH=disk\/QM:memory/ s/$/:SharedMemory/' /root/tests/FFI/Makefile
RUN sed -i '/^cratetestqm:/ s/$/ tst_shm/' /root/tests/FFI/Makefile
RUN sed -i '/^cratetestasil:/ s/$/ tst_sys_shm/' /root/tests/FFI/Makefile
RUN sed -i '/clean:/i tst_sys_shm: shm.c' /root/tests/FFI/Makefile
RUN sed -i '/^tst_sys_shm:/a\	$(CC) $(CFLAGS_SHM) -o $(ASIL_BIN)\/$@ $<' /root/tests/FFI/Makefile
RUN sed -i '/clean:/i tst_shm: shm.c' /root/tests/FFI/Makefile
RUN sed -i '/^tst_shm:/a\	$(CC) $(CFLAGS_SHM) -o $(QM_BIN)\/$@ $<' /root/tests/FFI/Makefile

# memory
RUN curl --create-dirs -O --output-dir /root/tests/FFI/memory/ https://raw.githubusercontent.com/containers/qm/main/tests/e2e/tools/FFI/memory/memory_eat.c

# disk - file-allocate.c
RUN mkdir -p /root/tests/FFI/memory/{QM,ASIL}  /root/tests/FFI/disk/{QM,ASIL}
RUN curl --create-dirs -O --output-dir /root/tests/FFI/disk/QM/ https://raw.githubusercontent.com/containers/qm/main/tests/e2e/tools/FFI/disk/QM/file-allocate.c

# bluechi-tester
RUN curl --create-dirs -O --output-dir /root/tests/FFI/bin/ https://raw.githubusercontent.com/eclipse-bluechi/bluechi/main/tests/tools/FFI/bluechi-tester
RUN chmod +x /root/tests/FFI/bin/bluechi-tester

# shared memory
RUN curl --create-dirs -O --output-dir /root/tests/FFI/SharedMemory/ https://gitlab.com/redhat/edge/tests/base-image/-/raw/main/FFI/tests/containers/SharedMemory/shm.c

# Build all C programs
RUN cd /root/tests/FFI && \
    make all

# setting bluechi and qm
CMD if [ "$shortbuild" = "" ]; then \
	COPY files/ /; fi

RUN if [ "$shortbuild" = "" ]; then \
        gem install ruby-dbus; \
        npm install markdownlint-cli2 --global ; \
	curl https://raw.githubusercontent.com/containers/qm/main/setup > /usr/share/qm/setup && \
chmod + /usr/share/qm/setup; \
	usr/share/qm/setup --skip-systemctl --hostname localrootfs; \
	systemctl enable bluechi-controller; \
	systemctl enable bluechi-agent; \
    fi

WORKDIR /root/tests/FFI/bin

CMD /sbin/init
