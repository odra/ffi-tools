# Free-From-Interference

Free-From-Interference (FFI) means applications within the QM environment do not interfere with applications in the ASIL environment.

**How to build and run this image**:

``` sh
# podman build -f Containerfile --cap-add=sys_admin -t localhost/ffi-tools:latest .
# podman run --rm -d --name ffi-tools --privileged localhost/ffi-tools:latest
```
Note: the container needs to be built using `--cap-add=sys_admin` because of the qm setup script.

**Thin image**:

In case of testing only, add the following `--build-arg shortbuild=yes` to build command

``` sh
# podman build -f Containerfile --build-arg shortbuild=yes --cap-add=sys_admin -t localhost/ffi-tools:latest .
```
**Using**:

To "sh into the container" use:
``` sh
# podman exec -it ffi-tools bash
```

## Memory
QM environment will allocates 90% or greater of memory of the system. 
Launch application in the ASIL environment that requires > 10% of memory.
Make sure swap is turned off.
Make sure OOM Killer kills QM applications.

Example:
- memory/ASIL/20_percent_cpu_eat.c 
- memory/QM/90_percent_cpu_eat.c 

## Disk
Try to allocate as maximum possible disk space.
